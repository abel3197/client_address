import { React, Component } from 'react';

export default class AddressesModal extends Component {
	state = {}
	componentWillMount() { }
	render() {
		const { client, addresses } = this.props;
		return (
			<div class="modal fade" id="addressModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel">Direcciones del cliente {client.name}</h4>
						</div>
						<div class="modal-body">
							<table class="table">
								<thead>
									<tr>
										<th scope="col">#</th>
										<th scope="col">Provincia</th>
										<th scope="col">Ciudad</th>
										<th scope="col">Sector</th>
										<th scope="col">Barrio</th>
										<th scope="col">Calle</th>
										<th scope="col">Número</th>
										<th scope="col">Referencia</th>
										<th scope="col"></th>
									</tr>
								</thead>
								<tbody>
									{addresses.map((address, idx) => (
										<tr key={idx}>
											<th scope="row">{address.id}</th>
											<td>{address.province}</td>
											<td>{address.city}</td>
											<td>{address.district}</td>
											<td>{address.neighborhood}</td>
											<td>{address.street}</td>
											<td>{address.number}</td>
											<td>{address.reference}</td>
										</tr>
									))}
								</tbody>
							</table>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-primary">Save changes</button>
						</div>
					</div>
				</div>
			</div>
		);
	}
}