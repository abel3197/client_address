import './App.css';
import { Component } from 'react';
// import AddressesModal from './AddressesModal';
// import ContactsModal from './ContactsModal';

class App extends Component {
	state = {
		clients: [{
			"id": 1, "name": "Carlos Pérez",
			"document_type": "Cédula", "document": "001-2345678-9", "creation_date": "2021-07-01", "gender": "Masculino",
			"address": [{ "id": 2, "client_id": 1, "province": "Distrito Nacional", "city": "Santo Domingo de Guzmán", "district": "El Vergel", "neighborhood": "Vergel", "street": "Av. Máximo Gómez", "number": "87", "reference": "Al frente de la DNCD" }], "contacts": []
		}, { "id": 2, "name": "María Gómez", "document_type": "Pasaporte", "document": "A098765432", "creation_date": "2021-07-02", "gender": "Femenino", "address": [], "contacts": [] }],
		client: {},
		addresses: [],
		contacts: [],
	}
	componentWillMount() {
		this.fetchClients();
	}
	fetchClients() {
		fetch('http://localhost:4000/clients')
			.then(response => {
				this.setState({
					clients: response.json()
				});
			});
	}
	render() {
		const { clients, client, addresses, contacts, } = this.state;
		return (
			<div className="App">
				<div class="page-header">
					<h1>Clientes de Oriontek</h1>
					<small>Sistema para administrar direcciones de clientes</small>
					<hr />
				</div>
				<br />
				<div class="panel panel-default">
					<table class="table">
						<thead>
							<tr>
								<th scope="col">#</th>
								<th scope="col">Nombre</th>
								<th scope="col">Tipo de documento</th>
								<th scope="col">Documento</th>
								<th scope="col">Fecha de creación</th>
								<th scope="col">Género</th>
								<th scope="col"></th>
								<th scope="col"></th>
							</tr>
						</thead>
						<tbody>
							{clients.map((client, idx) => (
								<tr key={idx} >
									<th scope="row">{client.id}</th>
									<td>{client.name}</td>
									<td>{client.document_type}</td>
									<td>{client.document}</td>
									<td>{client.creation_date}</td>
									<td>{client.gender}</td>
									<td>
										<button type="button" class="btn btn-primary btn-lg"
											data-toggle="modal" data-target="#addressModal"
											onClick={() => {
												this.setState({ addresses: client.address });
											}}
										>Direcciones</button>
									</td>
									<td>
										<button type="button" class="btn btn-primary btn-lg"
											data-toggle="modal" data-target="#contactModal"
											onClick={() => {
												this.setState({ addresses: client.contacts });
											}}
										>Contactos</button>
									</td>
								</tr>
							))}
						</tbody>
					</table>
				</div>
				{/* <AddressesModal client={client} addresses={addresses} /> */}
				{/* <ContactsModal client={client} addresses={contacts} /> */}
			</div>
		);
	};
}
export default App;
