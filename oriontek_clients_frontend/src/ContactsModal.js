import { React, Component } from 'react';

export default class AddressesModal extends Component {

    render() {
        const { client, addresses: contacts } = this.props;
        return (
            <div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Contactos del cliente {client.name}</h4>
                        </div>
                        <div class="modal-body">
					<table class="table">
						<thead>
							<tr>
								<th scope="col">#</th>
								<th scope="col">Tipo de contacto</th>
								<th scope="col">Contacto</th>
							</tr>
						</thead>
						<tbody>
							{contacts.map((contact, idx) => (
								<tr key={idx}>
									<th scope="row">{contact.id}</th>
									<td>{contact.contact_type}</td>
									<td>{contact.contact}</td>
								</tr>
							))}
						</tbody>
					</table>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary">Save changes</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}