package _do.oriontek.clients;

import _do.oriontek.clients.resource.ClientResource;
import _do.oriontek.clients.service.ClientService;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import javax.sql.DataSource;
import org.skife.jdbi.v2.DBI;

public class ClientApplication extends Application<AppConfiguration> {

    private static final String SQL = "sql";

    public static void main(String[] args) throws Exception {
        new ClientApplication().run("server", args[0]);
    }

    @Override
    public void initialize(Bootstrap<AppConfiguration> b) {
        //HTTP.postResource(httpClient, );
    }

    @Override
    public void run(AppConfiguration config, Environment env) throws Exception {
        // Datasource configuration
        final DataSource dataSource = config.getDataSourceFactory().build(env.metrics(), SQL);
        DBI dbi = new DBI(dataSource);
        env.jersey().register(new ClientResource(dbi.onDemand(ClientService.class)));
    }
}
