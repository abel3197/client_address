package _do.oriontek.clients.resource;

import _do.oriontek.clients.model.Client;
import _do.oriontek.clients.service.ClientService;

import com.codahale.metrics.annotation.Timed;
import com.wordnik.swagger.annotations.Api;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;

@Path("/clients")
@Produces(MediaType.APPLICATION_JSON)
@Api(value = "clients", description = "Clients Resource for performing CRUD operations on Clients Table")
public class ClientResource {

	private final ClientService clientService;

	public ClientResource(ClientService clientService) {
		this.clientService = clientService;
	}

	private Response makeCORS(Response.ResponseBuilder req, String returnMethod) {
		Response.ResponseBuilder rb = req
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, PUT, OPTIONS, DELETE");

		if (!"".equals(returnMethod)) {
			rb.header("Access-Control-Allow-Headers", returnMethod);
		}

		return rb.build();
	}

	@OPTIONS
//	@Path("{subResources:.*}")
	public Response corsMyResource(@HeaderParam("Access-Control-Request-Headers") String requestH) {
		return makeCORS(Response.ok(), requestH);
	}
	@OPTIONS
	@Path("{id}")
	public Response corsMyResource2(@HeaderParam("Access-Control-Request-Headers") String requestH) {
		return makeCORS(Response.ok(), requestH);
	}

	@GET
	@Timed
	public Response getClients(@HeaderParam("Access-Control-Request-Headers") String requestH) {
		return makeCORS(Response.ok(clientService.getClients()), requestH);
	}

	@GET
	@Timed
	@Path("{id}")
	public Response getClient(@PathParam("id") final int id, @HeaderParam("Access-Control-Request-Headers") String requestH) {
		return makeCORS(Response.ok(clientService.getClient(id)), requestH);
	}

	@POST
	@Timed
	public Response createClient(@NotNull final Client client, @HeaderParam("Access-Control-Request-Headers") String requestH) {
		Client created = clientService.createClient(client);
		return makeCORS(Response.ok(created), requestH);
	}

	@PUT
	@Timed
	@Path("{id}")
	public Response editClient(@NotNull @Valid final Client client,
			@PathParam("id") final int id, @HeaderParam("Access-Control-Request-Headers") String requestH) {
		client.setId(id);
		return makeCORS(Response.ok(clientService.editClient(client)), requestH);
	}

	@DELETE
	@Timed
	@Path("{id}")
	public Response deleteClient(@PathParam("id") final int id, @HeaderParam("Access-Control-Request-Headers") String requestH) {
		Map<String, String> response = new HashMap<>();
		response.put("status", clientService.deleteClient(id));
		return makeCORS(Response.ok(response), requestH);
	}
}
