package _do.oriontek.clients.service;

import _do.oriontek.clients.dao.AddressDao;
import _do.oriontek.clients.model.Client;
import org.skife.jdbi.v2.exceptions.UnableToExecuteStatementException;
import org.skife.jdbi.v2.exceptions.UnableToObtainConnectionException;
import org.skife.jdbi.v2.sqlobject.CreateSqlObject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;
import java.util.List;
import java.util.Objects;
import _do.oriontek.clients.dao.ClientDao;
import _do.oriontek.clients.dao.ContactDao;

public abstract class ClientService {
	//LOGS

	private static final String DATABASE_ACCESS_ERROR = "Could not reach the MySQL database. The database may be down or there may be network connectivity issues. Details: ";
	private static final String DATABASE_CONNECTION_ERROR = "Could not create a connection to the MySQL database. The database configurations are likely incorrect. Details: ";
	private static final String UNEXPECTED_DATABASE_ERROR = "Unexpected error occurred while attempting to reach the database. Details: ";
	private static final String SUCCESS = "Success";
	private static final String UNEXPECTED_DELETE_ERROR = "An unexpected error occurred while deleting cliente.";
	private static final String CLIENT_NOT_FOUND = "Client id %s not found.";

	@CreateSqlObject
	abstract ClientDao clientDao();

	@CreateSqlObject
	abstract AddressDao addressDao();

	@CreateSqlObject
	abstract ContactDao contactDao();

	public List<Client> loadChildren(List<Client> clients) {
		for (Client client : clients) {
			loadChildren(client);
		}
		return clients;
	}

	public Client loadChildren(Client client) {
		client.setAddress(addressDao().getClientAddresses(client.getId()));
		client.setContacts(contactDao().getClientContacts(client.getId()));
		return client;
	}

	public List<Client> getClients() {
		List<Client> clients = clientDao().getClients();
		return loadChildren(clients);
	}

	public Client getClient(int id) {
		Client client = clientDao().getClient(id);
		if (Objects.isNull(client)) {
			throw new WebApplicationException(String.format(CLIENT_NOT_FOUND, id), Status.NOT_FOUND);
		}
		return loadChildren(client);
	}

	public Client createClient(Client cliente) {
		clientDao().createClient(cliente);
		return getClient(clientDao().lastInsertId());
	}

	public Client editClient(Client cliente) {
		if (Objects.isNull(clientDao().getClient(cliente.getId()))) {
			throw new WebApplicationException(String.format(CLIENT_NOT_FOUND, cliente.getId()),
				Status.NOT_FOUND);
		}
		clientDao().editClient(cliente);
		return getClient(cliente.getId());
	}

	public String deleteClient(final int id) {
		int result = clientDao().deleteClient(id);
		switch (result) {
			case 1:
				return SUCCESS;
			case 0:
				throw new WebApplicationException(String.format(CLIENT_NOT_FOUND, id), Status.NOT_FOUND);
			default:
				throw new WebApplicationException(UNEXPECTED_DELETE_ERROR, Status.INTERNAL_SERVER_ERROR);
		}
	}

	public String performHealthCheck() {
		try {
			clientDao().getClients();
		} catch (UnableToObtainConnectionException ex) {
			return checkUnableToObtainConnectionException(ex);
		} catch (UnableToExecuteStatementException ex) {
			return checkUnableToExecuteStatementException(ex);
		} catch (Exception ex) {
			return UNEXPECTED_DATABASE_ERROR + ex.getCause().getLocalizedMessage();
		}
		return null;
	}

	private String checkUnableToObtainConnectionException(UnableToObtainConnectionException ex) {
		if (ex.getCause() instanceof java.sql.SQLNonTransientConnectionException) {
			return DATABASE_ACCESS_ERROR + ex.getCause().getLocalizedMessage();
		} else if (ex.getCause() instanceof java.sql.SQLException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return UNEXPECTED_DATABASE_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

	private String checkUnableToExecuteStatementException(UnableToExecuteStatementException ex) {
		if (ex.getCause() instanceof java.sql.SQLSyntaxErrorException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return UNEXPECTED_DATABASE_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

}
