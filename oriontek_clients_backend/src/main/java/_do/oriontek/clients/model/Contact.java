/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package _do.oriontek.clients.model;

public class Contact {

	private Integer id;
	private Integer client_id;
	private String contact_type;
	private String contact;

	public Contact() {
	}

	public Contact(Integer id, Integer client_id, String contact_type, String contact) {
		this.id = id;
		this.client_id = client_id;
		this.contact_type = contact_type;
		this.contact = contact;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getClient_id() {
		return client_id;
	}

	public void setClient_id(Integer client_id) {
		this.client_id = client_id;
	}

	public String getContact_type() {
		return contact_type;
	}

	public void setContact_type(String contact_type) {
		this.contact_type = contact_type;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

}
