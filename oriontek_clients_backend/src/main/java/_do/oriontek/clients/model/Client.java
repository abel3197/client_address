/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package _do.oriontek.clients.model;

//import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class Client {

	private Integer id;
	private String name;
	private String document_type;
	private String document;
	private String creation_date;
	private String gender;

	private List<Address> address;
	private List<Contact> contacts;

	public Client() {
	}

	public Client(Integer id, String name, String document_type, String document, String creation_date, String gender) {
		this.id = id;
		this.name = name;
		this.document_type = document_type;
		this.document = document;
		this.creation_date = creation_date;
		this.gender = gender;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDocument_type() {
		return document_type;
	}

	public void setDocument_type(String document_type) {
		this.document_type = document_type;
	}

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public String getCreation_date() {
		return creation_date;
	}

	public void setCreation_date(String creation_date) {
		this.creation_date = creation_date;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public List<Address> getAddress() {
		return address;
	}

	public void setAddress(List<Address> address) {
		this.address = address;
	}

	public List<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(List<Contact> contacts) {
		this.contacts = contacts;
	}

}
