/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package _do.oriontek.clients.model;

public class Address {

	private Integer id;
	private Integer client_id;
	private String province;
	private String city;
	private String district;
	private String neighborhood;
	private String street;
	private String number;
	private String reference;

	public Address() {
	}

	public Address(Integer id, Integer client_id, String province, String city, String district, String neighborhood, String street, String number, String reference) {
		this.id = id;
		this.client_id = client_id;
		this.province = province;
		this.city = city;
		this.district = district;
		this.neighborhood = neighborhood;
		this.street = street;
		this.number = number;
		this.reference = reference;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getClient_id() {
		return client_id;
	}

	public void setClient_id(Integer client_id) {
		this.client_id = client_id;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getNeighborhood() {
		return neighborhood;
	}

	public void setNeighborhood(String neighborhood) {
		this.neighborhood = neighborhood;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

}
