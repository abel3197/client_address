package _do.oriontek.clients.dao;

import _do.oriontek.clients.model.Client;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

@RegisterMapper(ClientDao.ClientMapper.class)
public interface ClientDao {

	@SqlQuery("select * from clients;")
	public List<Client> getClients();

	@SqlQuery("select * from clients where id = :id")
	public Client getClient(@Bind("id") final int id);

	@SqlUpdate("INSERT INTO clients (name, document_type, document, creation_date, gender)"
		+ " VALUES (:name, :document_type, :document, :creation_date, :gender)")
	void createClient(@BindBean final Client client);

	@SqlUpdate("update clients set"
		+ " name=:name,"
		+ " document_type=:document_type,"
		+ " document=:document,"
		+ " creation_date=:creation_date,"
		+ " gender=:gender "
		+ "where id = :id")
	void editClient(@BindBean final Client client);

	@SqlUpdate("delete from clients where id = :id")
	int deleteClient(@Bind("id") final int id);

	@SqlQuery("select last_insert_id();")
	public int lastInsertId();

	static public class ClientMapper implements ResultSetMapper<Client> {

		private static final String ID = "id";
		private static final String NAME = "name";
		private static final String DOCUMENT_TYPE = "document_type";
		private static final String DOCUMENT = "document";
		private static final String CREATION_DATE = "creation_date";
		private static final String GENDER = "gender";
		
		@Override
		public Client map(int i, ResultSet resultSet, StatementContext sContext) throws SQLException {
			HashMap<String, Boolean> hasColumn = new HashMap();
			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columns = rsmd.getColumnCount();
			for (int x = 1; x <= columns; x++) {
				hasColumn.put(rsmd.getColumnName(x).toLowerCase(), Boolean.TRUE);
			}
			Client client = new Client(
				hasColumn.containsKey(ID) ? resultSet.getInt(ID) : 0,
				hasColumn.containsKey(NAME) ? resultSet.getString(NAME) : "",
				hasColumn.containsKey(DOCUMENT_TYPE) ? resultSet.getString(DOCUMENT_TYPE) : "",
				hasColumn.containsKey(DOCUMENT) ? resultSet.getString(DOCUMENT) : "",
				hasColumn.containsKey(CREATION_DATE) ? resultSet.getString(CREATION_DATE) : "",
				hasColumn.containsKey(GENDER) ? resultSet.getString(GENDER) : ""
			);
			return client;
		}

	}
}
