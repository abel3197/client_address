package _do.oriontek.clients.dao;

import _do.oriontek.clients.model.Address;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

@RegisterMapper(AddressDao.AddressMapper.class)
public interface AddressDao {

	@SqlQuery("select * from addresses;")
	public List<Address> getAddresses();

	@SqlQuery("select * from addresses where client_id = :client_id")
	public List<Address> getClientAddresses(@Bind("client_id") final int client_id);

	@SqlQuery("select * from addresses where id = :id")
	public Address getAddress(@Bind("id") final int id);

	@SqlUpdate("INSERT INTO addresses (client_id, province, city, district, neighborhood, street, number, reference)"
		+ " VALUES (:client_id, :province, :city, :district, :neighborhood, :street, :number, :reference)")
	void createAddress(@BindBean final Address address);

	@SqlUpdate("update addresses set"
		+ " client_id=:client_id,"
		+ " province=:province,"
		+ " city=:city,"
		+ " district=:district,"
		+ " neighborhood=:neighborhood,"
		+ " street=:street,"
		+ " number=:number,"
		+ " reference=:reference "
		+ "where id = :id")
	void editAddress(@BindBean final Address address);

	@SqlUpdate("delete from addresses where id = :id")
	int deleteAddress(@Bind("id") final int id);

	@SqlQuery("select last_insert_id();")
	public int lastInsertId();

	static public class AddressMapper implements ResultSetMapper<Address> {

		private static final String ID = "id";
		private static final String CLIENT_ID = "client_id";
		private static final String PROVINCE = "province";
		private static final String CITY = "city";
		private static final String DISTRICT = "district";
		private static final String NEIGHBORHOOD = "neighborhood";
		private static final String STREET = "street";
		private static final String NUMBER = "number";
		private static final String REFERENCE = "reference";

		@Override
		public Address map(int i, ResultSet resultSet, StatementContext sContext) throws SQLException {
			HashMap<String, Boolean> hasColumn = new HashMap();
			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columns = rsmd.getColumnCount();
			for (int x = 1; x <= columns; x++) {
				hasColumn.put(rsmd.getColumnName(x).toLowerCase(), Boolean.TRUE);
			}
			Address address = new Address(
				hasColumn.containsKey(ID) ? resultSet.getInt(ID) : 0,
				hasColumn.containsKey(CLIENT_ID) ? resultSet.getInt(CLIENT_ID) : 0,
				hasColumn.containsKey(PROVINCE) ? resultSet.getString(PROVINCE) : "",
				hasColumn.containsKey(CITY) ? resultSet.getString(CITY) : "",
				hasColumn.containsKey(DISTRICT) ? resultSet.getString(DISTRICT) : "",
				hasColumn.containsKey(NEIGHBORHOOD) ? resultSet.getString(NEIGHBORHOOD) : "",
				hasColumn.containsKey(STREET) ? resultSet.getString(STREET) : "",
				hasColumn.containsKey(NUMBER) ? resultSet.getString(NUMBER) : "",
				hasColumn.containsKey(REFERENCE) ? resultSet.getString(REFERENCE) : ""
			);
			return address;
		}

	}
}
