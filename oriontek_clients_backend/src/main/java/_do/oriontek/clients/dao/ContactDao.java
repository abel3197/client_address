package _do.oriontek.clients.dao;

import _do.oriontek.clients.model.Contact;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

@RegisterMapper(ContactDao.ContactMapper.class)
public interface ContactDao {

	@SqlQuery("select * from contacts;")
	public List<Contact> getContactes();

	@SqlQuery("select * from contacts where client_id = :client_id")
	public List<Contact> getClientContacts(@Bind("client_id") final int client_id);

	@SqlQuery("select * from contacts where id = :id")
	public Contact getContact(@Bind("id") final int id);

	@SqlUpdate("INSERT INTO contacts (client_id, contact_type, contact)"
		+ " VALUES (:client_id, :contact_type, :contact)")
	void createContact(@BindBean final Contact contact);

	@SqlUpdate("update contacts set"
		+ " client_id=:client_id, contact_type=:contact_type, contact=:contact "
		+ "where id = :id")
	void editContact(@BindBean final Contact contact);

	@SqlUpdate("delete from contacts where id = :id")
	int deleteContact(@Bind("id") final int id);

	@SqlQuery("select last_insert_id();")
	public int lastInsertId();

	static public class ContactMapper implements ResultSetMapper<Contact> {

		private static final String ID = "id";
		private static final String CLIENT_ID = "client_id";
		private static final String CONTACT_TYPE = "contact_type";
		private static final String CONTACT = "contact";

		@Override
		public Contact map(int i, ResultSet resultSet, StatementContext sContext) throws SQLException {
			HashMap<String, Boolean> hasColumn = new HashMap();
			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columns = rsmd.getColumnCount();
			for (int x = 1; x <= columns; x++) {
				hasColumn.put(rsmd.getColumnName(x).toLowerCase(), Boolean.TRUE);
			}
			Contact contact = new Contact(
				hasColumn.containsKey(ID) ? resultSet.getInt(ID) : 0,
				hasColumn.containsKey(CLIENT_ID) ? resultSet.getInt(CLIENT_ID) : 0,
				hasColumn.containsKey(CONTACT_TYPE) ? resultSet.getString(CONTACT_TYPE) : "",
				hasColumn.containsKey(CONTACT) ? resultSet.getString(CONTACT) : ""
			);
			return contact;
		}

	}
}
